package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, Integer id, int num) {

    	//statementはSQL文（以下プログラム）をデータベース（MySQL）を送る箱の役割
        PreparedStatement ps = null;
        try {
        	//StringBuilderを作ることで文字の羅列を作ることができる
            StringBuilder sql = new StringBuilder();

            	sql.append("SELECT ");
                sql.append("    messages.id as id, ");
                sql.append("    messages.text as text, ");
                sql.append("    messages.user_id as user_id, ");
                sql.append("    users.account as account, ");
                sql.append("    users.name as name, ");
                sql.append("    messages.created_date as created_date ");
                sql.append("FROM messages ");
                //messagesテーブルとusersテーブルを内部結合するSQL文
                sql.append("INNER JOIN users ");
                //messages.user_idとusers.user.idの紐づけ
                sql.append("ON messages.user_id = users.id ");
                //もし、user_id(仮）user_id = 4)をクリックした場合 = nullではない→ バインド変数をsqlへappend
                //↑idに値が入ってきた場合は↑
                if(id != null) {
                	sql.append(" WHERE user_id = ? ");
                }
                //numに入ってくるのは1000?
                sql.append("ORDER BY created_date DESC limit " + num);

            //初期値（null)に前述プログラム（SELECT文をsqlへappend）を文字列にしてpsに格納
            ps = connection.prepareStatement(sql.toString());
            //ユーザー選択(aタグをクリック)した場合、psに値をセット（指定ユーザーの投稿のみを指定）
            if(id != null) {
            	//psのプレースホルダー（今回はWHERE user_id = ?）の値にuser_id =4をint型でセット
            	//WHERE以下に２つ以上プレースホルダーがある場合は番号を増やす
            	ps.setInt(1, id);
            }

            //psをMySQLで実行し、rsへ格納
            ResultSet rs = ps.executeQuery();

            //実行結果をtop.jspを経由してブラウザで表示
            List<UserMessage> messages = toUserMessages(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setAccount(rs.getString("account"));
				message.setName(rs.getString("name"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}
                return messages;

        } finally {
            close(rs);
        }
    }
}